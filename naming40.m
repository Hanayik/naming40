function catcherror = naming40
testing = false;
catcherror = 0;
[subj, runnum, fmri] = getSessionInfo; if isempty(subj); return; end
if strcmpi(fmri,'key')
    fmri = 'space';
end

if strcmpi(subj,'999')
    testing = true;
end

oldLevel = Screen('Preference', 'VisualDebugLevel', 1);
if testing
    Screen('Preference', 'SkipSyncTests', 2);
    fmri = 'space';
else
    Screen('Preference', 'SkipSyncTests',0);
end
KbName('UnifyKeyNames');
picdir = fullfile(fileparts(mfilename('fullpath')),['Stimuli' filesep]);
datadir = fullfile(fileparts(mfilename('fullpath')),['data' filesep]);
if ~exist(datadir,'dir')
    mkdir(datadir);
end
T = readtable(fullfile(fileparts(mfilename('fullpath')),'Naming40StimuliInOrder.csv'));
datestring = getDateAndTime;
datafile = fullfile(datadir,sprintf('%s_%s_%s.txt',subj, runnum, datestring));
%fid = fopen(fullfile(datadir,sprintf('%s_%s_%s.txt',subj, runnum, datestring)),'w+');
%fprintf(fid,'subj\trun\tstimulus\tImgtype\twait1\twait2\timgDur\timgOn\timgOff\ttrialDuration\n');
PsychDefaultSetupPlus(3);
params = PsychSetupParams(0,1);
instruct = 'Please name the items in the pictures. Do not name the abstract pictures';
n = size(T,1); % number of trials is the number of rows in Table T
s = struct;
s.subj(1:n,1) = {subj};
s.run(1:n,1) = {runnum};
s.picname(1:n,1) = {''};
s.imgtype(1:n,1) = {''};
s.frontpad(1:n,1) = 0;
s.backpad(1:n,1) = 0;
s.imgDur(1:n,1) = 0;
s.imgOnsetTime(1:n,1) = 0;
s.imgOffsetTime(1:n,1) = 0;
s.totalTrialDuration(1:n,1) = 0;
dur = 2-params.ifi;
trialDur = 10-params.ifi; % 10 secs
startTime = WaitForScannerStart(params,instruct, fmri);
try
    RestrictKeysForKbCheck(KbName('escape'));
    params.topPriorityLevel = MaxPriority(params.win);
    Priority(params.topPriorityLevel);
    for i=1:n
        %tstart = GetSecs; % trial start time
%         if KbCheck
%             CleanUp;
%         end
        frontPad = (T.wait1(i)/1000);
        backPad = (T.wait2(i)/1000) - params.ifi;
        picname = deblank(char(T.pic(i)));
        if strfind(lower(picname),lower('abs'))
            imgtype = 'abs';
        else
            imgtype = 'exp';
        end
        imgname = fullfile(picdir,[deblank(char(T.pic(i))) '.bmp']);
        [tstart, imgOnset, imgOffset,tend] = showStimulus(params, imgname, dur, frontPad, backPad);
        WaitSecs('UntilTime',tstart + trialDur);
        %tend = GetSecs;
        %fprintf(fid,'%s\t%s\t%s\t%s\t%g\t%g\t%g\t%g\t%g\t%g\n',subj,runnum,picname,imgtype,frontPad,backPad,dur,imgOnset-startTime,imgOffset-startTime,tend-tstart);
        %fprintf('%s\t%s\t%s\t%s\t%g\t%g\t%g\t%g\t%g\t%g\n',subj,runnum,picname,imgtype,frontPad,backPad,dur,imgOnset-startTime,imgOffset-startTime,tend-tstart);
        s.picname(i,1) = {picname};
        s.imgtype(i,1) = {imgtype};
        s.frontpad(i,1) = frontPad;
        s.backpad(i,1) = backPad;
        s.imgDur(i,1) = dur;
        s.imgOnsetTime(i,1) = imgOnset-startTime;
        s.imgOffsetTime(i,1) = imgOffset-startTime;
        s.totalTrialDuration(i,1) = tend-tstart;
        
    end
    %fclose(fid);
    t = struct2table(s);
    writetable(t, datafile, 'Delimiter', '\t');
    Screen('Preference', 'VisualDebugLevel', oldLevel);
    CleanUp;
    
catch catcherror
    t = struct2table(s);
    writetable(t, datafile, 'Delimiter', '\t');
    CleanUp
end
end
%sub functions below





















function datestring = getDateAndTime
d = fix(clock);
datestring=sprintf('%s%s%s%s%s%s',...
    num2str(d(1)),...
    num2str(d(2)),...
    num2str(d(3)),...
    num2str(d(4)),...
    num2str(d(5)),...
    num2str(d(6)));
end

% function startTime = ShowInstructions(params, instruct,keyToWaitFor)
% if nargin < 2; keyToWaitFor = 'space'; end;
% DrawFormattedText(params.win, instruct, 'center', 'center',params.TextColor);
% Screen('Flip',params.win);
% RestrictKeysForKbCheck(KbName(keyToWaitFor));
% deviceN = -1;
% startTime = KbWait(deviceN);
% RestrictKeysForKbCheck([]);
% %startTime = WaitForScannerStart(params, fmri);
% end


function expStartTime = WaitForScannerStart(params, instruct, keyToWaitFor)
if nargin < 1; keyToWaitFor = 'space'; end;
DrawFormattedText(params.win,instruct, 'center', 'center',params.TextColor);
Screen('Flip',params.win);
if strfind(lower(keyToWaitFor),'xmr')
    a=instrfind;
    if isempty(a)~=1
        fclose(a);
    end
    port = serial('COM4', 'BaudRate', 19200);
    set(port,'Timeout',100000);
    fopen(port);
    while ( uint32(fread(port,1)) ~= '5'); end; %XMR
    fclose(port);
    
elseif strfind(lower(keyToWaitFor),'kki')
    a=instrfind;
    if isempty(a)~=1
        fclose(a);
    end
    port = serial('COM4', 'BaudRate', 19200);
    set(port,'Timeout',100000);
    fopen(port);
    while ( uint32(fread(port,1)) ~= '6'); end; %KKI
    fclose(port);
else
    RestrictKeysForKbCheck(KbName(keyToWaitFor));
    deviceN = -1;
    KbWait(deviceN);
    KbReleaseWait(deviceN);
    RestrictKeysForKbCheck([]);
end
expStartTime = Screen('Flip',params.win);
end %WaitForScannerStart

function CleanUp
ListenChar(0);
sca;
end %CleanUp

function [vbl, imgOnset, imgOffset, tend] = showStimulus(params,imgname, dur, frontPad, backPad)
%showStimulus(params, img, dur, frontPad, backPad);
%vbl  = Screen('Flip', params.win, vbl + (waitframes - 0.5) * params.ifi); % Flip to the screen
img = imread(imgname);
imgsize = size(img);
params.picRect = [0 0 imgsize(2) imgsize(1)];
params.picRect = CenterRectOnPoint(params.picRect,params.Xc,params.Yc);
tex = Screen('MakeTexture', params.win, img);
vbl = Screen('Flip',params.win);
Screen('DrawTexture', params.win, tex, [], [], 0);
%imgOnset = Screen('Flip', params.win, vbl+frontPad);
imgOnset = Screen('Flip', params.win,vbl + (((frontPad/params.ifi) - 0.5) * params.ifi));
%vbl = [];
%imgOnset = Screen('Flip', params.win);
imgOffset = Screen('Flip', params.win, imgOnset + (((dur/params.ifi) - 0.5) * params.ifi));
tend = Screen('Flip', params.win, imgOffset + (((backPad/params.ifi) - 0.5) * params.ifi));
%Screen('Flip', params.win, imgOffset+ backPad);
%Screen('Flip',params.win, backPadOff+2);
Screen('Close', tex);
clear img;
end

function PsychDefaultSetupPlus(featureLevel)
% PsychDefaultSetup(featureLevel) - Perform standard setup for Psychtoolbox.

% Default colormode to use: 0 = clamped, 0-255 range. 1 = unclamped 0-1 range.
global psych_default_colormode;
psych_default_colormode = 0;

% Reset KbName mappings:
clear KbName;

% Define maximum supported featureLevel for this Psychtoolbox installation:
maxFeatureLevel = 3;

% Sanity check featureLevel argument:
if nargin < 1 || isempty(featureLevel) || ~isscalar(featureLevel) || ~isnumeric(featureLevel) || featureLevel < 0
    error('Mandatory featureLevel argument missing or invalid (not a scalar number or negative).');
end

% Always AssertOpenGL:
AssertOpenGL;

% Level 1+ requested?
if featureLevel >= 1
    % Unify keycode to keyname mapping across operating systems:
    KbName('UnifyKeyNames');
end

% Level 2+ requested?
if featureLevel >= 2
    % Initial call to timing functions
    % Set global environment variable to ask PsychImaging() to enable
    % normalized color range for all drawing commands and Screen('MakeTexture'):
    psych_default_colormode = 1;
    GetSecs; WaitSecs(0.001);
end

% Level 2+ requested?
if featureLevel >= 3
    %suppress keypress to command window,
    %and hide the mouse pointer (usefull is most visual experiments)
    ListenChar(2);
    HideCursor;
end


if featureLevel > maxFeatureLevel
    error('This installation of Psychtoolbox can not execute scripts at the requested featureLevel of %i, but only up to level %i ! UpdatePsychtoolbox!', featureLevel, maxFeatureLevel);
end
return;
end

function params = PsychSetupParams(doAlphaBlending,doMultiSample)
%sets up some normal values used in experiments such as a gray background
%and Arial font, and a large text size, etc...
%saves all relevant screen info to the 'params' structure so that the
%entire structure can be passed in and out of functions, rather than
%zillions of variables. Also makes it expandable.
%
% History:
% 29-May-2015   th     made initial version of the function

global psych_default_colormode;
%make params structure
params = struct;
%set some defualt, common colors
params.colors.white = [1 1 1];
params.colors.black = [0 0 0];
params.colors.gray = [0.5 0.5 0.5];
params.colors.red = [1 0 0];
params.colors.green = [0 1 0];
%check if using normalized color values or not
if psych_default_colormode == 0
    params.colors.white = [255 255 255];
    params.colors.gray = [128 128 128];
end
%choose max screen number (will be the external monitor if connected)
params.screen = max(Screen('Screens'));
params.font = 'Arial'; %set the global font for PTB to use
params.tsize = 18; %set text size
params.TextColor = [params.colors.white]; %set global text color
%set the background color of the screen (defaults to gray)
params.background = params.colors.black;
params.multiSample = [];
if doMultiSample
    params.multiSample = 4;%set to a value greater than 0 if you want super sampling
end
%open the PTB window
params.win = PsychImaging('OpenWindow', params.screen, params.background,[],[],[],[],params.multiSample);
%get screen width and height
[params.maxXpixels, params.maxYpixels] = Screen('WindowSize', params.win);
if doAlphaBlending
    %Set blend function for alpha blending
    Screen('BlendFunction', params.win, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');
end
%find center of screen
[params.Xc,params.Yc] = RectCenter([0 0 params.maxXpixels params.maxYpixels]);
%now that the window pointer exists, set some values from earlier
Screen('TextSize', params.win, params.tsize);
Screen('TextFont',params.win, params.font);
Screen('TextSize',params.win, params.tsize);
Screen('TextStyle', params.win, 1);

%Maximum priority level
params.topPriorityLevel = MaxPriority(params.win);
Priority(params.topPriorityLevel);
%Query the frame duration
params.ifi = Screen('GetFlipInterval', params.win);
end


function [subj, runnum, fmri] = getSessionInfo
subj = [];
runnum = [];
fmri = [];
prompt={'Participant: ','Session: ', 'MRI (key, kki, xmr): '};
   name='Naming 40 Task';
   numlines=1;
   defaultanswer={'0','0','key'};
answer=inputdlg(prompt,name,numlines,defaultanswer);
if isempty(answer); return; end
subj = answer{1};
runnum = answer{2};
fmri = answer{3};
end


